# Go 并发模式

#### 介绍
Google IO 2012 Go Concurrency Patterns

[Google IO 2012 Go Concurrency Patterns](https://www.youtube.com/watch?v=f6kdp27TYZs)

[b站中文字幕版](https://www.bilibili.com/video/BV1UJ411m7U1/)

[PPT](https://talks.golang.org/2012/concurrency.slide#1)

[代码来源](https://talks.golang.org/2012/concurrency/support)

我进行了部分改动


#### 安装教程

```shell
git clone https://gitee.com/frankyu365/google-io-2012-go-concurrency-patterns.git 
```

#### 使用说明

1.  cd xxx/main
3.  go run xxx.go

#### 对应博客

[Go-并发模式1(Basic Examples)](https://blog.csdn.net/lady_killer9/article/details/120144685)

[Go-并发模式2（Patterns）](https://blog.csdn.net/lady_killer9/article/details/120145534)
