package main

import (
	"fmt"
	"time"
)

func main() {
	var c1, c2, c3 chan int
	// modified by lady_killer9
	c1 = make(chan int)
	c3 = make(chan int)
	go func() {c1 <- 1}()
	time.Sleep(time.Second)
	// if you add some loop to select10, you will find after "received 1 from c1", it will print the default case
	// by the way, we can not input something to a nil channel !!!
	select {
	case v1 := <-c1:
		fmt.Printf("received %v from c1\n", v1)
	case v2 := <-c2:
		fmt.Printf("received %v from c2\n", v2) // I change it to v2
	case c3 <- 23:
		fmt.Printf("sent %v to c3\n", 23)
	default:
		fmt.Printf("no one was ready to communicate\n")
	}
}
